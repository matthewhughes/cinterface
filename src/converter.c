#include <stddef.h>
#include <string.h>
#include "converter.h"

#define BASE_CONVERTER(_name) extern const struct BaseConverterOps \
    _name##_converter;
BASE_CONVERTER(bin)
BASE_CONVERTER(hex)
#undef BASE_CONVERTER

/* Converter implementations */
static const struct BaseConverterOps *converter_ops[] = {
    &bin_converter,
    &hex_converter,
    NULL,
};

const struct BaseConverterOps *converter_get_ops(const char *str)
{
    const struct BaseConverterOps **ops = converter_ops;

    if (!str || *str == '\0')
        return NULL;

    for (; *ops; ops++)
        if (strcmp(str, (*ops)->name) == 0)
            return *ops;

    return NULL;
}

bool is_valid_converter(const char *str)
{
    return converter_get_ops(str) != NULL;
}
