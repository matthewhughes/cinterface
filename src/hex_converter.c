#include "converter.h"

#define HEX_POW 4
#define HEX_CHARSET_SIZE (1 << HEX_POW)

static const char hex_charset[HEX_CHARSET_SIZE] = {
    '0', '1', '2', '3',
    '4', '5', '6', '7',
    '8', '9', 'a', 'b',
    'c', 'd', 'e', 'f'
};

static int hex_to_int(const char *hex_str) {
    int retval = 0;
    for (const char *p = hex_str; *p != '\0'; p++) {
        retval <<= HEX_POW;
        for (int i = 0; i < HEX_CHARSET_SIZE; i++) {
            if (*p == hex_charset[i]) {
                retval += i;
                break;
            }
            if (i == HEX_CHARSET_SIZE - 1) {
                ; /* TODO: error */
            }
        }
    }
    return retval;
}

static void hex_from_dec(unsigned dec, char *dest) {
    int digit_count = 1;
    int tmp = dec;

    while (tmp >>= HEX_POW)
        digit_count++;

    for (int i = 0; i < digit_count; i++)
        dest[digit_count - (i + 1)] = hex_charset[(dec >> (i * HEX_POW)) % 16];
    dest[digit_count] = '\0';
}

BASE_CONVERTER_DEF(hex)
