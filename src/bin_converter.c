#include "converter.h"

static int bin_to_int(const char *bin_str) {
    int retval = 0;
    for (const char *p = bin_str; *p; p++) {
        retval <<= 1;
        if (*p == '1')
            retval += 1;
        else if (*p != '0') {
            ; /* TODO: error */
        }
    }
    return retval;
}

static void bin_from_dec(unsigned dec, char *dest) {
    int digit_count =  1;
    int tmp = dec;
    while (tmp >>= 1)
        digit_count++;

    for (int i = 0; i < digit_count; i++)
        dest[digit_count - (i + 1)] = (dec >> i) % 2 ? '1' : '0';
    dest[digit_count] = '\0';
}

BASE_CONVERTER_DEF(bin)
