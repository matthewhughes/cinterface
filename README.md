# An Interface in C

Playing around with an interface pattern. The interface is is a simple
converter between integer and string representations of a number in a given
base. See include/converter.h for details on the implementation

Tests (require [check](https://libcheck.github.io/check/)) via `make test`.
