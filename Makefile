SRC_DIR = src
INC_DIR = include
TARGET = convert
TEST_TARGET = test_convert
TEST_SRC = t/main.c

WARNINGS = -pedantic -Wall -Wextra
INCLUDES = -I$(INC_DIR)
CFLAGS = -g $(WARNINGS) $(INCLUDES)
LDLIBS = -lm

TEST_LDFLAGS = $(shell pkg-config --cflags --libs check) -lcheck

SRCS = $(wildcard $(SRC_DIR)/*.c)
HEADERS = $(wildcard $(INC_DIR)/*.h)
OBJS = $(SRCS:.c=.o)

.PHONY: test clean

$(TEST_TARGET): $(OBJS) $(TEST_SRC)
	$(CC) $(CFLAGS) -o $@ $^ $(TEST_LDFLAGS)

test: $(TEST_TARGET)
	./$(TEST_TARGET)

clean:
	-rm -f $(OBJS) $(TEST_TARGET)
