#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include "converter.h"

struct TestData {
    int val;
    char *bin;
    char *oct;
    char *hex;
};

#define MAX_STR_SIZE 1024

static const struct TestData tests[] = {
    {
        .val = 0,
        .bin = "0",
        .oct = "0",
        .hex = "0",
    },
    {
        .val = 1,
        .bin = "1",
        .oct = "1",
        .hex = "1",
    },
    {
        .val = 2,
        .bin = "10",
        .oct = "2",
        .hex = "2",
    },
    {
        .val = 8,
        .bin = "1000",
        .oct = "10",
        .hex = "8",
    },
    {
        .val = 8,
        .bin = "1000",
        .oct = "10",
        .hex = "8",
    },
    {
        .val = 16,
        .bin = "10000",
        .oct = "20",
        .hex = "10",
    },
    {
        .val = 127,
        .bin = "1111111",
        .oct = "177",
        .hex = "7f",
    },
};

START_TEST (test_bin_to_int) {
    const struct BaseConverterOps *ops = converter_get_ops("bin");
    const int got = ops->to_int(tests[_i].bin);
    ck_assert_int_eq(got, tests[_i].val);
}
END_TEST

START_TEST (test_bin_from_int) {
    char res[MAX_STR_SIZE];
    const struct BaseConverterOps *ops = converter_get_ops("bin");
    ops->from_dec(tests[_i].val, res);
    ck_assert_str_eq(res, tests[_i].bin);
}
END_TEST

START_TEST (test_hex_to_int) {
    const struct BaseConverterOps *ops = converter_get_ops("hex");
    const int got = ops->to_int(tests[_i].hex);
    ck_assert_int_eq(got, tests[_i].val);
}
END_TEST

START_TEST (test_hex_from_int) {
    char res[MAX_STR_SIZE];
    const struct BaseConverterOps *ops = converter_get_ops("hex");
    ops->from_dec(tests[_i].val, res);
    ck_assert_str_eq(res, tests[_i].hex);
}
END_TEST

START_TEST (test_is_valid_converter) {
    ck_assert(is_valid_converter("bin"));
    ck_assert(!is_valid_converter("INVALID_BASE"));
}
END_TEST

Suite *is_valid_converter_suit(void) {
    Suite *s = suite_create("is_valid_converter");
    TCase *tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_is_valid_converter);
    suite_add_tcase(s, tc_core);

    return s;
}

Suite *bin_suit(void) {
    Suite *s = suite_create("bin conversions");
    TCase *tc_core = tcase_create("Core");

    const size_t test_count = sizeof(tests)/sizeof(tests[0]);
    tcase_add_loop_test(tc_core, test_bin_to_int, 0, test_count);
    tcase_add_loop_test(tc_core, test_bin_from_int, 0, test_count);
    suite_add_tcase(s, tc_core);

    return s;
}

Suite *hex_suit(void) {
    Suite *s = suite_create("hex conversions");
    TCase *tc_core = tcase_create("Core");

    const size_t test_count = sizeof(tests)/sizeof(tests[0]);
    tcase_add_loop_test(tc_core, test_hex_to_int, 0, test_count);
    tcase_add_loop_test(tc_core, test_hex_from_int, 0, test_count);
    suite_add_tcase(s, tc_core);

    return s;
}

int run_bin_suite(void) {
    Suite *s = bin_suit();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    return srunner_ntests_failed(sr);
}

int run_hex_suite(void) {
    Suite *s = hex_suit();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    return srunner_ntests_failed(sr);
}

int run_is_valid_converter_suit(void) {
    Suite *s = is_valid_converter_suit();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    return srunner_ntests_failed(sr);
}

int main(void)
{
    int number_failed = run_bin_suite() + run_hex_suite() + run_is_valid_converter_suit();
    
    return number_failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
    return 0;
}
