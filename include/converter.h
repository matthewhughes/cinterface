#ifndef _INTERFACE_H

#include <stdbool.h>

/* Steps to add a new implementation for base <name>:
 *  1. Add add src/<name>_converter with methods:
 *      * <name>_to_int
 *      * <name>_from_dec
 *  2. Add a CONVERTER_DEF(<name>) at the end of the file
 *  3. Add BASE_CONVERTER(<name>) where the macro is defined in src/converter.c
 *  4. Add &<name>_converter entry to converter_ops in src/converter.c
 */

/* Interface to implment */
struct BaseConverterOps {
  const char *name;
  int (*to_int)(const char *);
  void (*from_dec)(unsigned, char *);
};

/* Get implementation for a given type, e.g. converter_get_ops(bin) */
const struct BaseConverterOps *converter_get_ops(const char *);
bool is_valid_converter(const char *);

/* Helper to declare an implementation */
#define BASE_CONVERTER_DEF(_name)                                              \
  const struct BaseConverterOps _name##_converter = {                          \
      .name = #_name,                                                          \
      .to_int = _name##_to_int,                                                \
      .from_dec = _name##_from_dec,                                            \
  };
#endif /* _INTERFACE_H */
